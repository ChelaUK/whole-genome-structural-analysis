# If you adapt this script for your own use, you will need to set these two variables based on your environment.
# SV_DIR is the installation directory for SVToolkit - it must be an exported environment variable.
# SV_TMPDIR is a directory for writing temp files, which may be large if you have a large data set.
#export SV_DIR=`cd .. && pwd`
SV_TMPDIR=./tmpdir

runDir=test1
bam=BGI-262_combined_R1.a.fq.gz.bwa.bam.sorted.rmdup.bam
sites=test1.discovery.vcf
genotypes=test1.genotypes.vcf

# These executables must be on your path.
which java > /dev/null || exit 1
which Rscript > /dev/null || exit 1
which samtools > /dev/null || exit 1

# For SVAltAlign, you must use the version of bwa compatible with Genome STRiP.
export PATH=${SV_DIR}/bwa:${PATH}
export LD_LIBRARY_PATH=${SV_DIR}/bwa:${LD_LIBRARY_PATH}

mx="-Xmx4g"
classpath="${SV_DIR}/lib/SVToolkit.jar:${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar:${SV_DIR}/lib/gatk/Queue.jar"

#mkdir -p ${runDir}/logs || exit 1
#mkdir -p ${runDir}/metadata || exit 1

# Unzip the reference sequence and masks if necessary
#if [ ! -e data/human_b36_chr1.fasta -a -e data/human_b36_chr1.fasta.gz ]; then
#    gunzip data/human_b36_chr1.fasta.gz
#fi
#if [ ! -e data/human_b36_chr1.svmask.fasta -a -e data/human_b36_chr1.svmask.fasta.gz ]; then
#    gunzip data/human_b36_chr1.svmask.fasta.gz
#fi
#if [ ! -e data/human_b36_chr1.gcmask.fasta -a -e data/human_b36_chr1.gcmask.fasta.gz ]; then
#    gunzip data/human_b36_chr1.gcmask.fasta.gz
#fi

# Display version information.
java -cp ${classpath} ${mx} -jar ${SV_DIR}/lib/SVToolkit.jar

# Run preprocessing.
# For large scale use, you should use -reduceInsertSizeDistributions, but this is too slow for the installation test.
# The method employed by -computeGCProfiles requires a GC mask and is currently only supported for human genomes.
java -cp ${classpath} ${mx} \
    org.broadinstitute.gatk.queue.QCommandLine \
    -S ${SV_DIR}/qscript/SVPreprocess.q \
    -S ${SV_DIR}/qscript/SVQScript.q \
    -gatk ${SV_DIR}/lib/gatk/GenomeAnalysisTK.jar \
    --disableJobReport \
    -cp ${classpath} \
    -configFile conf/genstrip_parameters.txt \
    -tempDir ${SV_TMPDIR} \
    -R /share/apps/genomics/reference/human_g1k_v37.fasta \
    -genomeMaskFile /share/apps/genomics/reference/masks/human_g1k_v37.mask.100.fasta \
    -copyNumberMaskFile /share/apps/genomics/reference/masks/cn2_mask_g1k_v37.fasta  \
    -genderMapFile gendermap.map \
    -runDirectory ${runDir} \
    -md ${runDir}/metadata \
    -disableGATKTraversal \
    -useMultiStep \
    -reduceInsertSizeDistributions false \
    -computeGCProfiles true \
    -computeReadCounts true \
    -jobLogDir ${runDir}/logs \
    -I ${bam} \
    -run
